import java.util.Scanner;

public class PuissanceQuatre {
    private char[][] plateau;
    private char joueur1, joueur2;
    private boolean estJoueur1;
    private Scanner scanner;

    public PuissanceQuatre() {
        plateau = new char[6][7];
        joueur1 = 'O';
        joueur2 = 'X';
        estJoueur1 = true;
        scanner = new Scanner(System.in);

        initPlat();
        while(!finDuJeu()){
            afficherPlat();
            int res;
            do{
                res = saisieJoueur();
            }while(!saisieValide(res));
            updatePlateau(res);
        }
        System.out.println( ((estJoueur1)? joueur2: joueur1) + " à gagné!");
        afficherPlat();

    }

    void initPlat(){
        for(int ligne = 0; ligne < 6; ligne++){
            for(int colonne = 0; colonne < 7; colonne++){
                plateau[ligne][colonne] = ' ';
            }
        }
    }

    void afficherPlat(){

        for(int ligne = 6; ligne > 0; ligne--){
            for(int colonne = 0; colonne < 7; colonne++){
                System.out.print(plateau[ligne-1][colonne] + "\t");
            }
            System.out.println();
        }
        System.out.println("-\t-\t-\t-\t-\t-\t-");
        System.out.println("0\t1\t2\t3\t4\t5\t6");
    }

    int saisieJoueur(){
        int res = 0;
        do {
            System.out.print( ((estJoueur1)? joueur1: joueur2) + " : Saisir un chiffre entre 0 et 6 : ");
            String str = scanner.nextLine();
            try{
                res = Integer.parseInt(str);
            }catch(NumberFormatException e){
                res = -1;
            }
        }while(res < 0 || res > 6);
        return res;
    }

    boolean saisieValide(int indiceColonne){
        if(plateau[5][indiceColonne] == ' '){
            return true;
        }else{
            return false;
        }
    }

    void updatePlateau(int saisie){
        for(int i = 0; i < 6; i++){
            if(plateau[i][saisie] == ' '){
                plateau[i][saisie] = (estJoueur1)? joueur1 : joueur2;
                break;
            }
        }
        estJoueur1 = !estJoueur1;
    }

    boolean finDuJeu(){
        return alignementGagnant() || plateauEstComplet();
    }

    boolean alignementGagnant(){
        boolean res = false;
        for(int ligne = 0; ligne < 6; ligne++){
            for(int colonne = 0; colonne < 7; colonne++){
                if(plateau[ligne][colonne] != ' '){//If not empty
                    if(ligne < 3){
                        if(plateau[ligne][colonne] == plateau[ligne+1][colonne] && plateau[ligne][colonne] == plateau[ligne+2][colonne] && plateau[ligne][colonne] == plateau[ligne+3][colonne]){
                            res = true;
                            break;
                        }
                    }
                    if(colonne < 4){
                        if(plateau[ligne][colonne] == plateau[ligne][colonne+1] && plateau[ligne][colonne] == plateau[ligne][colonne+2] && plateau[ligne][colonne] == plateau[ligne][colonne+3]){
                            res = true;
                            break;
                        }
                    }
                    if(ligne < 3 && colonne < 4){
                        if(plateau[ligne][colonne] == plateau[ligne+1][colonne+1] && plateau[ligne][colonne] == plateau[ligne+2][colonne+2] && plateau[ligne][colonne] == plateau[ligne+3][colonne+3]){
                            res = true;
                            break;
                        }
                    }
                    if(ligne > 2 && colonne < 4){
                        if(plateau[ligne][colonne] == plateau[ligne-1][colonne+1] && plateau[ligne][colonne] == plateau[ligne-2][colonne+2] && plateau[ligne][colonne] == plateau[ligne-3][colonne+3]){
                            res = true;
                            break;
                        }
                    }
                }
            }
        }
        return res;
    }

    boolean plateauEstComplet(){
        int res = 0;
        for(int colonne = 0; colonne < 7; colonne++) {
            if(plateau[5][colonne] != ' '){
                res++;
            }
        }
        return res == 7;
    }
}
